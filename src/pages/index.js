import React from 'react'
// import '../styles/index.css'
import { withStyles } from '@material-ui/core/styles';
import { BrowserRouter as Router } from 'react-router-dom';


import SideBar from '../base/Sidebar';
import Body from '../base/Body';
import NavBar from '../base/Navbar'

import CssBaseline from '@material-ui/core/CssBaseline';

import useStyles from '../styles/withStyles'

class App extends React.Component {

    state = {
        sideBar: true
    }



    handleSideBar = () => {
        this.state.sideBar ? this.setState({sideBar: false}) : this.setState({sideBar: true}) 
      }
    
    
    render(){
        const { classes } = this.props;
        return (
         
        <div className={classes.root}>
            <CssBaseline />
                <NavBar
                handleSideBar={this.handleSideBar}
                open={this.state.sideBar}
                />
              <Router>
                <SideBar 
                  handleSideBar={this.handleSideBar}
                  open={this.state.sideBar}
                />
                <Body 
                    open={this.state.sideBar}
                />
              </Router>
        </div>
          
          );
        
    }
}


export default withStyles(useStyles)(App);