import Dashboard from '../pages/Dashboard'

export const routes = [
    {
        path: "/",
        exact: true,
        main: () => <Dashboard />
    }


]


export const SidebarMenu = [
    {
        to: '/',
        label: 'Dashboard'
    },
    {
        to: '/Users',
        label: 'Users'
    }
]
